package persistencia.dao.interfaz;

import java.util.List;

import dto.IngresoEgresoDTO;
import utils.ReadForAnio;
import utils.ReadForAnioMes;
import utils.ReadGeneral;

public interface IngresoEgresoDAO 
{

	public boolean insert (IngresoEgresoDTO ingresoEgreso);

	public boolean update (IngresoEgresoDTO ingresoEgreso);
	
	public boolean delete (IngresoEgresoDTO ingresoEgreso);
	
	public List<IngresoEgresoDTO> readAll ();
	
	public IngresoEgresoDTO readForId (String idIngresoEgreso);

	public List<ReadGeneral> readGeneral ();
	
	public List<ReadForAnio> readForAnio (String anio);

	public List<ReadForAnioMes> readForAnioMes ();

	public List<IngresoEgresoDTO> readOfMonth (String anio, String mes);
		
}