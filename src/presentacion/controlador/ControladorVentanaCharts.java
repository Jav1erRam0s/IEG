package presentacion.controlador;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import modelo.GestorIngresosEgresos;
import utils.ReadForAnio;
import utils.ReadGeneral;
import utils.TableGeneral;
import utils.TableMes;

public class ControladorVentanaCharts 
{
	@FXML
	public BorderPane borderPane;
	@FXML
	public Label lblNameChart;
	@FXML
	public GridPane gridChart;

	@FXML
	public Label lblIngresos;
	@FXML
	public Label lblEgresos;
	@FXML
	public Label lblDeficit;
	
	@FXML
	public TableView<TableGeneral> tblGeneral;
	@FXML
	private TableColumn<String, String> colAnioHeader;
	@FXML
	private TableColumn<TableGeneral, String> colAnio;
	@FXML
	private TableColumn<TableGeneral, String> colAnioPerdidas;
	@FXML
	private TableColumn<TableGeneral, String> colAnioGanancias;

	@FXML
	public TableView<TableMes> tblMes;
	@FXML
	private TableColumn<String, String> colMesHeader;
	@FXML
	private TableColumn<TableMes, String> colMes;
	@FXML
	private TableColumn<TableMes, String> colMesPerdidas;
	@FXML
	private TableColumn<TableMes, String> colMesGanancias;

	@FXML
	public Button btnVolver;
	
	private BorderPane borderPaneVentanaPrincipal;
	public String anio;
	
	public ObservableList<TableGeneral> tableGeneral;
	public ObservableList<TableMes> tableMes;
	
	public List<ReadGeneral> readGeneral;	
	public List<ReadForAnio> readForAnio;

	public Tooltip tooltip = new Tooltip();
	
	@SuppressWarnings("deprecation")
	public void initialize(BorderPane borderPaneVentanaPrincipal)
	{
		this.borderPaneVentanaPrincipal = borderPaneVentanaPrincipal;
		this.tooltip.setStyle("-fx-text-fill: orange;");
		
		this.borderPane.setStyle(" -fx-background-image: url('resources/images/fondoIE.jpg'); -fx-background-size: cover; ");

		Image img2 = new Image("resources/images/volver.png");
	    ImageView view2 = new ImageView(img2);
	    view2.setFitHeight(20);
	    view2.setFitWidth(20);
	    this.btnVolver.setGraphic(view2);
	    
		// LLenado la tabla
		Date date = new Date();
        ZoneId timeZone = ZoneId.systemDefault();
        LocalDate getLocalDate = date.toInstant().atZone(timeZone).toLocalDate();
        this.anio = String.valueOf( getLocalDate.getYear() );
	    
		this.readGeneral = GestorIngresosEgresos.getInstance().readGeneral();
		this.readForAnio = GestorIngresosEgresos.getInstance().readForAnio( this.anio );

    	this.tableGeneral = FXCollections.observableArrayList( this.transformDataGral( this.readGeneral ) );
		this.tblGeneral.setItems(this.tableGeneral);
		this.tableMes = FXCollections.observableArrayList( this.transformDataAnio( this.readForAnio ) );
		this.tblMes.setItems(this.tableMes);

		this.tblGeneral.getStylesheets().add("styles/tblCharts.css");
		this.tblMes.getStylesheets().add("styles/tblCharts.css");
		
		this.colAnio.setCellValueFactory(new PropertyValueFactory<>("anio"));
		this.colAnioPerdidas.setCellValueFactory(new PropertyValueFactory<>("perdida"));
		this.colAnioGanancias.setCellValueFactory(new PropertyValueFactory<>("ganancia"));
		
		this.colMesHeader.setText( this.anio );
		this.colMes.setCellValueFactory(new PropertyValueFactory<>("mes"));
		this.colMesPerdidas.setCellValueFactory(new PropertyValueFactory<>("perdida"));
		this.colMesGanancias.setCellValueFactory(new PropertyValueFactory<>("ganancia"));
	
		// Deshabilitar el reordenamiento de las columnas.
		this.colAnioHeader.impl_setReorderable(false);
		this.colAnio.impl_setReorderable(false);
		this.colAnioPerdidas.impl_setReorderable(false);
		this.colAnioGanancias.impl_setReorderable(false);
	
		// Deshabilitar el reordenamiento de las columnas.
		this.colMesHeader.impl_setReorderable(false);
		this.colMes.impl_setReorderable(false);
		this.colMesPerdidas.impl_setReorderable(false);
		this.colMesGanancias.impl_setReorderable(false);
				
		this.colAnioHeader.getStyleClass().add("encabezado");
		this.colAnioHeader.setStyle(" -fx-background-color: #000000fa; -fx-alignment: center; ");
		this.colAnio.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colAnioPerdidas.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colAnioGanancias.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");

		this.colMesHeader.getStyleClass().add("encabezado");
		this.colMesHeader.setStyle(" -fx-background-color: #000000fa; -fx-alignment: center; ");
		this.colMes.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colMesPerdidas.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		this.colMesGanancias.setStyle(" -fx-text-fill : #000000; -fx-alignment: center; ");
		
		this.llenarChart();

		//this.colorearTablaAnio();
		this.colorearTablaMes();
	}

	/*
	private void colorearTablaAnio() {
        Callback<TableColumn<TableGeneral, String>, TableCell<TableGeneral, String>> cellFactory
                = new Callback<TableColumn<TableGeneral, String>, TableCell<TableGeneral, String>>() 
        {
            @Override
            public TableCell<TableGeneral, String> call(final TableColumn<TableGeneral, String> param) 
            {
                final TableCell<TableGeneral, String> cell = new TableCell<TableGeneral, String>() 
                {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) 
                        {
                            setGraphic(null);
                            setText(null);
                        } 
                        else 
                        {
                            setText(item);
                            @SuppressWarnings("unchecked")
							TableRow<TableGeneral> row = getTableRow();
                            if ( row.getItem() != null && Double.parseDouble( row.getItem().getGanancia().replace(',', '.') ) < 0) 
                            { row.setStyle(" -fx-background-color: #f4d03fb0; "); }
                        }
                    }
                };
                return cell;
            }
        };
        this.colAnioGanancias.setCellFactory(cellFactory);
    }
	*/

	private void colorearTablaMes() {
        Callback<TableColumn<TableMes, String>, TableCell<TableMes, String>> cellFactory
                = new Callback<TableColumn<TableMes, String>, TableCell<TableMes, String>>() 
        {
            @Override
            public TableCell<TableMes, String> call(final TableColumn<TableMes, String> param) 
            {
                final TableCell<TableMes, String> cell = new TableCell<TableMes, String>() 
                {
                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (empty) 
                        {
                            setGraphic(null);
                            setText(null);
                        } 
                        else 
                        {
                            setText(item);
                            @SuppressWarnings("unchecked")
							TableRow<TableMes> row = getTableRow();
                            if ( row.getItem() != null && Double.parseDouble( row.getItem().getGanancia().replace(',', '.') ) == 0 ) 
                            	{ row.setStyle(" -fx-background-color: #f4d03fb0; "); }
                        }
                    }
                };
                return cell;
            }
        };
        this.colMesGanancias.setCellFactory(cellFactory);
    }
	
	private List<TableGeneral> transformDataGral(List<ReadGeneral> registers)
	{
		ArrayList<TableGeneral> tableGral = new ArrayList<TableGeneral>();
		for (ReadGeneral data : registers)
		{
			TableGeneral newData = new TableGeneral();
			newData.setAnio(data.getAnio());
			Double perdida = (-data.getEgreso().doubleValue() / data.getIngreso().doubleValue() ) * 100;
			Double ganancia = (data.getGanancia().doubleValue() / data.getIngreso().doubleValue() ) * 100;
			newData.setPerdida( String.format("%,.2f", perdida) );
			newData.setGanancia( String.format("%,.2f", ganancia) );
			tableGral.add(newData);
		}
		return tableGral;
	}

	private List<TableMes> transformDataAnio(List<ReadForAnio> registers)
	{
		ArrayList<TableMes> tableGral = new ArrayList<TableMes>();
		for (ReadForAnio data : registers)
		{
			TableMes newData = new TableMes();
			newData.setMes( this.obtenerMesString( String.valueOf(data.getMes()) ) );
			Double perdida;
			Double ganancia;
			if( data.getIngreso().doubleValue() == 0.0 && data.getEgreso().doubleValue() != 0.0 )
			{
				perdida = 100.00;
				ganancia = 0.00;
			}
			else if( data.getEgreso().doubleValue() == 0.0 && data.getIngreso().doubleValue() != 0.0 )
			{
				perdida = 0.00;
				ganancia = 100.00;				
			}
			else
			{
				perdida = (-data.getEgreso().doubleValue() / data.getIngreso().doubleValue() ) * 100;
				ganancia = (data.getGanancia().doubleValue() / data.getIngreso().doubleValue() ) * 100;
			}
			if ( perdida > 100.00 && ganancia < 0.00 )
			{
				perdida = ( ( -data.getEgreso().doubleValue() - data.getIngreso().doubleValue() ) / -data.getEgreso().doubleValue() ) * 100;
				ganancia = 0.00;	
			}
			newData.setPerdida( String.format("%,.2f", perdida) );
			newData.setGanancia( String.format("%,.2f", ganancia) );
			tableGral.add(newData);
		}
		return tableGral;
	}

	private String obtenerMesString(String mes)
	{
		if ( mes.equals("1") ) { return "Enero"; }
		else if ( mes.equals("2") ) { return "Febrero"; }
		else if ( mes.equals("3") ) { return "Marzo"; }
		else if ( mes.equals("4") ) { return "Abril"; }
		else if ( mes.equals("5") ) { return "Mayo"; }
		else if ( mes.equals("6") ) { return "Junio"; }
		else if ( mes.equals("7") ) { return "Julio"; }
		else if ( mes.equals("8") ) { return "Agosto"; }
		else if ( mes.equals("9") ) { return "Septiembre"; }
		else if ( mes.equals("10") ) { return "Octubre"; }
		else if ( mes.equals("11") ) { return "Noviembre"; }
		else { return "Diciembre"; }
	}

	private int obtenerMesInt(String mes)
	{
		if ( mes.equals("Enero") ) { return 1; }
		else if ( mes.equals("Febrero") ) { return 2; }
		else if ( mes.equals("Marzo") ) { return 3; }
		else if ( mes.equals("Abril") ) { return 4; }
		else if ( mes.equals("Mayo") ) { return 5; }
		else if ( mes.equals("Junio") ) { return 6; }
		else if ( mes.equals("Julio") ) { return 7; }
		else if ( mes.equals("Agosto") ) { return 8; }
		else if ( mes.equals("Septiembre") ) { return 9; }
		else if ( mes.equals("Octubre") ) { return 10; }
		else if ( mes.equals("Noviembre") ) { return 11; }
		else { return 12; }
	}

	@FXML
	public void verAnalisisAnual(MouseEvent event) 
	{
		if( event.getClickCount() == 2 && this.tblGeneral.getSelectionModel().getSelectedItem() != null )
		{
			this.anio = String.valueOf( this.tblGeneral.getSelectionModel().getSelectedItem().getAnio() );
			this.colMesHeader.setText( this.anio );
			this.readForAnio = GestorIngresosEgresos.getInstance().readForAnio( this.anio );
			this.tableMes = FXCollections.observableArrayList( this.transformDataAnio( this.readForAnio ) );
			this.tblMes.setItems(this.tableMes);
			this.tblMes.refresh();
			this.llenarChart();
		}
	}
	
	@FXML
	public void volver(ActionEvent event) 
	{
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/presentacion/vista/VentanaGeneral.fxml"));
		try	
		{	
			Parent root = loader.load();
			
			ControladorVentanaGeneral contro = loader.getController();
			contro.initialize( this.borderPaneVentanaPrincipal );

			this.borderPaneVentanaPrincipal.setCenter(root);
			
			ControladorVentanaPrincipal.btnAnalisis.setDisable(false);
		}
		catch(Exception ex)	{	System.out.print(ex);	}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void llenarChart()
	{
		CategoryAxis xAxis = new CategoryAxis();
		NumberAxis yAxis = new NumberAxis();
	    AreaChart<String,Number> chart = new AreaChart<String,Number>(xAxis,yAxis);     

//	    yAxis.setLabel("Monto ($)");
	    DropShadow shadow = new DropShadow();
	    shadow.setOffsetX(2);
	    shadow.setColor(Color.GREY);
	    chart.setEffect(shadow);
	    yAxis.setTickLabelFormatter(new NumberAxis.DefaultFormatter(yAxis,"$ ",null));
	    
	    chart.setLegendVisible(false);

	    chart.getStylesheets().add("styles/charts.css");
	    
        XYChart.Series seriesIngresos= new XYChart.Series();
        XYChart.Series seriesEgresos = new XYChart.Series();
        
        List<ReadForAnio> listReadForAnio = new ArrayList<ReadForAnio>();
        for( int i = 1; i<=12; i++ )
        {
        	int j = -1;
        	int index = 0;
        	for(ReadForAnio data : this.readForAnio)
        	{
        		if( data.getMes() == i )
        		{ j = index; break; }
        		index++;
        	}
        	if( j != -1)
        	{
        		listReadForAnio.add(this.readForAnio.get(index));
        	}
        	else
        	{
        		ReadForAnio newRA = new ReadForAnio();
        		newRA.setMes(i);
        		newRA.setIngreso( new BigDecimal(0.00) );
        		newRA.setEgreso( new BigDecimal(0.00) );
        		newRA.setGanancia( new BigDecimal(0.00) );
        		listReadForAnio.add(newRA);
        	}
        }
        
        this.readForAnio = listReadForAnio;
        
        for( ReadForAnio data : this.readForAnio )
        {
        	seriesIngresos.getData().add(new XYChart.Data( this.obtenerMesString(String.valueOf(data.getMes())), data.getIngreso().doubleValue() ));
        	seriesEgresos.getData().add(new XYChart.Data( this.obtenerMesString(String.valueOf(data.getMes())), data.getEgreso().doubleValue()*-1 ));
        }
        
        chart.getData().addAll(seriesIngresos, seriesEgresos);

        this.lblNameChart.setText(this.anio);
        this.gridChart.getChildren().clear();
        this.gridChart.add(chart, 0, 0);   
        
        // Estilo color vertical.
        Node colorVertical = chart.lookup(".chart-vertical-grid-lines");
        colorVertical.setStyle(" -fx-stroke:  #5dade2; ");

        // Estilo de fondo de area
        Node fillI = seriesIngresos.getNode().lookup(".chart-series-area-fill");
        Node fillE = seriesEgresos.getNode().lookup(".chart-series-area-fill");
        fillI.setStyle(" -fx-stroke: #FFFFFF; -fx-fill: #52be80b0; ");
        fillE.setStyle(" -fx-stroke: #FFFFFF; -fx-fill:  #ec7063b0; ");
        
        // Estilo de color de linea        
        Node lineI = seriesIngresos.getNode().lookup(".chart-series-area-line");
        Node lineE = seriesEgresos.getNode().lookup(".chart-series-area-line");
        lineI.setStyle(" -fx-stroke: #000000b0; -fx-stroke-width: 2px; ");
        lineE.setStyle(" -fx-stroke: #000000b0; -fx-stroke-width: 2px; ");
        
        for (XYChart.Series<String, Number> s : chart.getData()) 
        {            
        	for (XYChart.Data<String, Number> d : s.getData()) 
            {
                Tooltip tooltip = new Tooltip();
                tooltip.setStyle(" -fx-text-fill: orange");
                tooltip.setText( "$ " + String.format("%,.2f", d.getYValue()) );
       
                Tooltip.install(d.getNode(), tooltip );
            }
        }
	}
	
	@FXML
	public void verGralPopUp( Event event ) 
	{ 
		if( this.tblGeneral.getSelectionModel().getSelectedIndex() != -1 )
		{
			int anio = this.tblGeneral.getSelectionModel().getSelectedItem().getAnio();
			String mensaje = "";
			for(ReadGeneral data : this.readGeneral)
			{
				if(data.getAnio() == anio) 
				{
					//mensaje = data.getAnio() + "\n" + "---" + "\n";
					mensaje = mensaje + "Ingresos : $ " + String.format("%,.2f", data.getIngreso()) + "\n";
					mensaje = mensaje + "Egresos : $ " + String.format("%,.2f", data.getEgreso()) + "\n";
					mensaje = mensaje + "Ganancias : $ " + String.format("%,.2f", data.getGanancia());
					break;
				}
			}
			this.tooltip.setText(mensaje);
			this.tblGeneral.setTooltip(tooltip);	
		}
	}

	@FXML
	public void verMesPopUp( Event event )
	{ 
		if( this.tblMes.getSelectionModel().getSelectedIndex() != -1 )
		{
			int mes = this.obtenerMesInt( this.tblMes.getSelectionModel().getSelectedItem().getMes() );
			String mensaje = "";
			for(ReadForAnio data : this.readForAnio)
			{
				if(data.getMes() == mes) 
				{
					//mensaje = this.tblMes.getSelectionModel().getSelectedItem().getMes() + "\n" + "---" + "\n";
					mensaje = mensaje + "Ingresos : $ " + String.format("%,.2f", data.getIngreso()) + "\n";
					mensaje = mensaje + "Egresos : $ " + String.format("%,.2f", data.getEgreso()) + "\n";
					mensaje = mensaje + "Ganancias : $ " + String.format("%,.2f", data.getGanancia());
					break;
				}
			}
			this.tooltip.setText(mensaje);
			this.tblMes.setTooltip(tooltip);	
		}
	}	
	
	@FXML
	public void infoIngresos( MouseEvent event )
	{
		String mensaje = "Representa la cantidad de dinero que entra a" + "\n";
		mensaje = mensaje + "formar parte de la econom�a de una persona.";
		this.tooltip.setText(mensaje);
		this.lblIngresos.setTooltip(tooltip);
	}
	
	@FXML
	public void infoEgresos( MouseEvent event )
	{
		String mensaje = "Representa la cantidad de dinero que sale y supone un" + "\n";
		mensaje = mensaje + "incremento (inversion) o una reducci�n del patrimonio.";
		this.tooltip.setText(mensaje);
		this.lblEgresos.setTooltip(tooltip);
	}
	
	@FXML
	public void infoDeficit( MouseEvent event )
	{
		String mensaje = "Situaci�n de la econom�a en la que" + "\n";
		mensaje = mensaje + "los gastos superan a los ingresos.";
		this.tooltip.setText(mensaje);
		this.lblDeficit.setTooltip(tooltip);
	}

}