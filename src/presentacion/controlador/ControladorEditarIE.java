package presentacion.controlador;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import modelo.GestorIngresosEgresos;

import java.math.BigDecimal;

import dto.IngresoEgresoDTO;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;

public class ControladorEditarIE
{

	@FXML
	private VBox panelIE;
	@FXML
	private Label lblTitulo;
	@FXML
	private Label lblMonto;
	@FXML
	private TextField txtTitulo;
	@FXML
	private TextArea txtDescripcion;
	@FXML
	private TextField txtMonto;
	@FXML
	private Button btnEditar;

	public IngresoEgresoDTO IE;
	public ControladorVentanaGestionIngresoEgreso contro;

	public void initialize( ControladorVentanaGestionIngresoEgreso contro ,IngresoEgresoDTO IE )
	{	
		this.contro = contro;
		this.IE = IE;		
		
		this.txtTitulo.setDisable(false);
		this.txtDescripcion.setDisable(false);
		this.txtMonto.setDisable(false);
		this.btnEditar.setDisable(false);
		
		if( IE.getMonto().doubleValue() > 0 )
		{	this.panelIE.setStyle("-fx-background-image: url('resources/images/ingreso.jpg'); -fx-background-size: cover; ");	}
		else
		{	this.panelIE.setStyle("-fx-background-image: url('resources/images/egreso.jpg'); -fx-background-size: cover; ");	}
		
		this.lblTitulo.setStyle("-fx-text-fill: #ffffff");
		this.lblMonto.setStyle("-fx-text-fill: #ffffff");
		
		this.txtTitulo.setText(IE.getTitulo());
		this.txtDescripcion.setText(IE.getDescripcion());
		this.txtMonto.setText(IE.getMonto().abs().toString());

		this.txtMonto.textProperty().addListener(new ChangeListener<String>() 
		{
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) 
            {
                if (!newValue.matches("\\d{0,10}([\\.]\\d{0,2})?")) 
                {
                    txtMonto.setText(oldValue);
                }
            }
        });
	}
	
	@FXML
	public void editarIE(ActionEvent event) 
	{
		if ( this.txtTitulo.getText().equals("") || this.txtMonto.getText().equals("") )
		{
			this.mostrarAlertError("Titulo y monto no pueden ser vacio.");
		}
		else
		{
			this.IE.setTitulo(this.txtTitulo.getText());
			this.IE.setDescripcion(this.txtDescripcion.getText());
			if( IE.getMonto().doubleValue() > 0 )
			{
				this.IE.setMonto( new BigDecimal(this.txtMonto.getText()) );
			}
			else
			{
				this.IE.setMonto( new BigDecimal("-"+this.txtMonto.getText()) ); 			
			}
			
			GestorIngresosEgresos.getInstance().update(this.IE);

			this.contro.ingresosEgresos.clear();
			this.contro.ingresosEgresosFiltrados.clear();
			this.contro.ingresosEgresos = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readOfMonth( this.contro.anio, this.contro.mes ) );
			this.contro.ingresosEgresosFiltrados = FXCollections.observableArrayList( GestorIngresosEgresos.getInstance().readOfMonth( this.contro.anio, this.contro.mes ) );
			
			this.contro.tblIngresosEgresos.setItems(this.contro.ingresosEgresosFiltrados);
			this.contro.tblIngresosEgresos.refresh();
			this.contro.txtFiltro.setText("");
			this.contro.actualizarTags();
			
			Stage stage = (Stage) this.btnEditar.getScene().getWindow();
			stage.close();
		}

	}
	
	private void mostrarAlertError(String mensaje) 
	{
	    Alert alert = new Alert(Alert.AlertType.ERROR);
	    alert.setHeaderText(null);
	    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("/resources/images/ieg.png"));
	    alert.setTitle("Error");
	    alert.setContentText( mensaje );
	    alert.showAndWait();
	}

}