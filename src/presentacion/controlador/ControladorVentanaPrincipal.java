package presentacion.controlador;

import java.io.File;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Screen;
import javafx.stage.Stage;
import utils.ImportExportBD;

public class ControladorVentanaPrincipal 
{

	@FXML
	private BorderPane borderPaneVentanaPrincipal;
	
	@FXML
	public Button btnSeccionIngresoEgreso;
	@FXML
	public Button btnSeccionAnalisis;
	@FXML
	public MenuButton MenuButtonBD;

	public static Button btnAnalisis;
	
	@SuppressWarnings("static-access")
	public void initialize()
	{
		 this.btnAnalisis = this.btnSeccionAnalisis;
		
	    Image img1 = new Image("resources/images/ingresos-egresos.png");
	    ImageView view1 = new ImageView(img1);
	    view1.setFitHeight(26);
	    view1.setFitWidth(26);
	    this.btnSeccionIngresoEgreso.setGraphic(view1);
	    
	    Image img2 = new Image("resources/images/analisis.png");
	    ImageView view2 = new ImageView(img2);
	    view2.setFitHeight(26);
	    view2.setFitWidth(26);
	    this.btnSeccionAnalisis.setGraphic(view2);
	    
		Image img3 = new Image("resources/images/base-de-datos.png");
	    ImageView view3 = new ImageView(img3);
	    view3.setFitHeight(26);
	    view3.setFitWidth(26);
		this.MenuButtonBD.setGraphic(view3);

		MenuItem menuItemImport = new MenuItem("     Importar     ");
		MenuItem menuItemExport = new MenuItem("     Exportar     ");
		menuItemImport.setOnAction(e -> this.importar());
		menuItemExport.setOnAction(e -> this.exportar());
		this.MenuButtonBD.getItems().addAll( menuItemImport, menuItemExport );
		
		String ui = "/presentacion/vista/VentanaGestionIngresoEgreso.fxml";

		FXMLLoader loader = new FXMLLoader(getClass().getResource(ui));
		try	
		{	
			Parent root = loader.load();
			this.borderPaneVentanaPrincipal.setCenter(root);
		}
		catch(Exception ex)	{	System.out.print(ex);	}
	}
	
	@FXML
	public void gestionIngresoEgreso(ActionEvent event) 
	{
		String ui = "/presentacion/vista/VentanaGestionIngresoEgreso.fxml";

		FXMLLoader loader = new FXMLLoader(getClass().getResource(ui));
		try	
		{	
			Parent root = loader.load();
			this.borderPaneVentanaPrincipal.setCenter(root);
			
			btnAnalisis.setDisable(false);
		}
		catch(Exception ex)	{	System.out.print(ex);	}
	}
	
	@FXML
	public void analisis(ActionEvent event) 
	{
		String ui = "/presentacion/vista/VentanaGeneral.fxml";
		FXMLLoader loader = new FXMLLoader(getClass().getResource(ui));
		try	
		{	
			Parent root = loader.load();
			
			ControladorVentanaGeneral contro = loader.getController();
			contro.initialize( this.borderPaneVentanaPrincipal );

			this.borderPaneVentanaPrincipal.setCenter(root);
		}
		catch(Exception ex)	{	System.out.print(ex);	}
	}

    public void importar() 
	{
		FileChooser select = new FileChooser();
		select.setTitle("Importar base de datos");
		String user = System.getProperty("user.name");
		File defaultDirectory = new File("C:/Users/"+user+"/Downloads");
		select.setInitialDirectory(defaultDirectory);
		select.getExtensionFilters().addAll( new FileChooser.ExtensionFilter("SQL", "*.sql") );
	
		Stage newStage = new Stage();
	
		File file = select.showOpenDialog( newStage );
	
	    Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
	    newStage.setX((screenBounds.getWidth() - newStage.getWidth()) / 2);
	    newStage.setY((screenBounds.getHeight() - newStage.getHeight()) / 2);
	
		if (file != null) 
		{
	        String fileAsString = file.toString();
	        if( ImportExportBD.Importar( fileAsString ) )
	        {
				this.mostrarAlertInformacion("ˇ Importación realizada exitosamente !");
				String ui = "/presentacion/vista/VentanaGestionIngresoEgreso.fxml";

				FXMLLoader loader = new FXMLLoader(getClass().getResource(ui));
				try	
				{	
					Parent root = loader.load();
					this.borderPaneVentanaPrincipal.setCenter(root);
					ControladorVentanaPrincipal.btnAnalisis.setDisable(false);
				}
				catch(Exception ex)	{	System.out.print(ex);	}
	        }
	        else
	        {
				this.mostrarAlertError("ˇ Ocurrio un error al importar !");
	        }
	    }
	}

	public void exportar() 
	{
//		DirectoryChooser select = new DirectoryChooser ();
//		select.setTitle("Exportar base de datos");
//		String user = System.getProperty("user.name");
//		File defaultDirectory = new File("C:/Users/"+user+"/Downloads");
//		select.setInitialDirectory(defaultDirectory);
//		
//		Stage newStage = new Stage();
//		File selectedDirectory = select.showDialog(newStage);
//	
//	    Rectangle2D screenBounds = Screen.getPrimary().getVisualBounds();
//	    newStage.setX((screenBounds.getWidth() - newStage.getWidth()) / 2);
//	    newStage.setY((screenBounds.getHeight() - newStage.getHeight()) / 2);
		
		String user = System.getProperty("user.name");
		File selectedDirectory = new File("C:/Users/"+user+"/Downloads");
		
		if (selectedDirectory != null) 
		{
	        String fileAsString = selectedDirectory.toString();
	        if( ImportExportBD.Exportar( fileAsString ) )
	        {
				this.mostrarAlertInformacion("ˇ Exportación realizada exitosamente !");            	
	        }
	        else
	        {
				this.mostrarAlertError("ˇ Ocurrio un error al exportar !");
	        }
	    }
	}
    
	private void mostrarAlertInformacion(String mensaje) 
	{
	    Alert alert = new Alert(Alert.AlertType.INFORMATION);
	    alert.setHeaderText(null);
	    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
	    stage.getIcons().add(new Image("/resources/images/ieg.png"));   
	    alert.setTitle("Info");
	    alert.setContentText( mensaje );
	    
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
		stage.setX( (primScreenBounds.getWidth() - 434) / 2 );
		stage.setY( (primScreenBounds.getHeight() - 142) / 2 );
	    
	    alert.showAndWait();
	}
	
	private void mostrarAlertError(String mensaje) 
	{
	    Alert alert = new Alert(Alert.AlertType.ERROR);
	    alert.setHeaderText(null);
	    Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image("/resources/images/ieg.png"));
	    alert.setTitle("Error");
	    alert.setContentText( mensaje );
	    
        Rectangle2D primScreenBounds = Screen.getPrimary().getVisualBounds();
		stage.setX( (primScreenBounds.getWidth() - 434) / 2 );
		stage.setY( (primScreenBounds.getHeight() - 142) / 2 );
	    
	    alert.showAndWait();
	}
	
}