package dto;

import java.math.BigDecimal;
import java.util.Date;

public class IngresoEgresoDTO 
{

	private String idIngresoEgreso;
	private String titulo;
	private String descripcion;
	private Date fecha;
	private BigDecimal monto;

	public String getIdIngresoEgreso() 
	{
		return idIngresoEgreso;
	}
	
	public void setIdIngresoEgreso(String idIngresoEgreso) 
	{
		this.idIngresoEgreso = idIngresoEgreso;
	}
	
	public String getTitulo() 
	{
		return titulo;
	}
	
	public void setTitulo(String titulo) 
	{
		this.titulo = titulo;
	}
	
	public String getDescripcion() 
	{
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) 
	{
		this.descripcion = descripcion;
	}
	
	public Date getFecha() 
	{
		return fecha;
	}
	
	public void setFecha(Date fecha) 
	{
		this.fecha = fecha;
	}
	
	public BigDecimal getMonto() 
	{
		return monto;
	}
	
	public void setMonto(BigDecimal monto) 
	{
		this.monto = monto;
	}
	
}