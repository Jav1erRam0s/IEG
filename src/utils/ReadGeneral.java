package utils;

import java.math.BigDecimal;

public class ReadGeneral 
{

	private int anio;
	private BigDecimal ingreso;
	private BigDecimal egreso;
	private BigDecimal ganancia;

	public int getAnio() 
	{
		return anio;
	}
	
	public void setAnio(int anio) 
	{
		this.anio = anio;
	}
	
	public BigDecimal getIngreso() 
	{
		return ingreso;
	}
	
	public void setIngreso(BigDecimal ingreso) 
	{
		this.ingreso = ingreso;
	}
	
	public BigDecimal getEgreso() 
	{
		return egreso;
	}
	
	public void setEgreso(BigDecimal egreso) 
	{
		this.egreso = egreso;
	}
	
	public BigDecimal getGanancia() 
	{
		return ganancia;
	}
	
	public void setGanancia(BigDecimal ganancia) 
	{
		this.ganancia = ganancia;
	}
	
}