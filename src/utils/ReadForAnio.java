package utils;

import java.math.BigDecimal;

public class ReadForAnio 
{

	private int mes;
	private BigDecimal ingreso;
	private BigDecimal egreso;
	private BigDecimal ganancia;

	public int getMes() 
	{
		return mes;
	}
	
	public void setMes(int mes) 
	{
		this.mes = mes;
	}
	
	public BigDecimal getIngreso() 
	{
		return ingreso;
	}
	
	public void setIngreso(BigDecimal ingreso) 
	{
		this.ingreso = ingreso;
	}
	
	public BigDecimal getEgreso() 
	{
		return egreso;
	}
	
	public void setEgreso(BigDecimal egreso) 
	{
		this.egreso = egreso;
	}
	
	public BigDecimal getGanancia() 
	{
		return ganancia;
	}
	
	public void setGanancia(BigDecimal ganancia) 
	{
		this.ganancia = ganancia;
	}
	
}