package utils;

public class TableGeneral 
{

	private int anio;
	private String perdida;
	private String ganancia;

	public int getAnio() 
	{
		return anio;
	}
	
	public void setAnio(int anio) 
	{
		this.anio = anio;
	}
	
	public String getPerdida() 
	{
		return perdida;
	}
	
	public void setPerdida(String perdida) 
	{
		this.perdida = perdida;
	}
	
	public String getGanancia() 
	{
		return ganancia;
	}
	
	public void setGanancia(String ganancia) 
	{
		this.ganancia = ganancia;
	}
		
}