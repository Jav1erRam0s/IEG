package main;

import java.io.IOException;
import java.net.ServerSocket;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Main  extends Application 
{

	public static ServerSocket SERVER_SOCKET;

	@Override
	public void start(Stage primaryStage) 
	{
		try 
		{	
			SERVER_SOCKET = new ServerSocket(20213); // a�o+app = 20213
			
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(Main.class.getResource("/presentacion/vista/VentanaPrincipal.fxml"));
			
			Pane ventana = (Pane) loader.load();
			
			Scene scene = new Scene(ventana);
			primaryStage.setScene(scene);
			
			primaryStage.getIcons().add(new Image("/resources/images/ieg.png"));
			primaryStage.setTitle("I - E = G");

			primaryStage.setMaximized(true);
			
			primaryStage.setMinWidth(900);
			primaryStage.setMinHeight(600);
			
			primaryStage.show();
			
			// Cerramos el socket al cerrar la ventana.
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			    @Override
			    public void handle(WindowEvent t) 
			    {
			    	try 
			    	{
						SERVER_SOCKET.close();
						System.exit(0);
					} 
			    	catch (IOException e) 
			    	{
			    		System.out.println( e.getMessage() );
					}
			    }
			});
		} 
		catch(Exception e) 
		{
    		System.out.println( e.getMessage() );
		}
	}
	
	public static void main(String[] args) 
	{
		launch(args);
	}

}